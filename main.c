#include "mem.h"

int main(void) {
    puts("heap_init");
    memalloc_debug_heap(stdout);

    puts("test1 malloc");
    int *test1 = _malloc(sizeof(int) * 100);
    for (int i = 0; i < 100; ++i) test1[i] = i+1;
    memalloc_debug_heap(stdout);

    puts("test2 malloc");
    int *test2 = _malloc(sizeof(int) * 100);
    memalloc_debug_heap(stdout);

    puts("test3 _realloc");
    test2 = _realloc(test2, sizeof(int) * 150);
    memalloc_debug_heap(stdout);

    puts("test1 _free");
    _free(test1);
    memalloc_debug_heap(stdout);


    puts("test2 _free");
    _free(test2);
    memalloc_debug_heap(stdout);

}
