CFLAGS = gcc -g -o
all: main

main: mem.o
	$(CFLAGS) main main.c mem.c -lm
mem.o:
	$(CFLAGS) mem -c mem.c 